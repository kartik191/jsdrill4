const defaults = require('../defaults.js');


let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = { name: undefined, age: 36, location: 'Gotham' ,23: defaults,'o':{a:'a',b:'b'},'a':[1,2,3],'f': function(){return 12;},g(){return 23;},h:()=>12};
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = "'{ name: 'Bruce Wayne', age: 36, location: 'Gotham' }'";
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = null;
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = undefined;
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = {};
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = true;
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = [1234, 'qwerty', 'mnob'];
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = (abc,xyz) => abc+xyz;
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));

testObject = new Set([1234, 'qwerty', 'mnob']);
console.log(defaults(testObject, {name: 'Bruce lee', age: 36, location: 'Gotham' , profesion:'mma', cont: 'china'}));