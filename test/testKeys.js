const keys = require('../keys.js');


let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(keys(testObject));

testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' ,23: keys,'o':{a:'a',b:'b'},'a':[1,2,3],'f': function(){return 12;},g(){return 23;},h:()=>12};
console.log(keys(testObject));

testObject = "'{ name: 'Bruce Wayne', age: 36, location: 'Gotham' }'";
console.log(keys(testObject));

testObject = null;
console.log(keys(testObject));

testObject = undefined;
console.log(keys(testObject));

testObject = 1234;
console.log(keys(testObject));

testObject = {};
console.log(keys(testObject));

testObject = [1234, 'qwerty', 'mnob'];
console.log(keys(testObject));

testObject = (abc,xyz) => abc+xyz;
console.log(keys(testObject));

testObject = new Set([1234, 'qwerty', 'mnob']);
console.log(keys(testObject));