const invert = require('../invert.js');


let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(invert(testObject));

testObject = { name: 'Bruce Wayne', age: 36, location: null ,23: invert,'o':{a:'a',b:{'b':23,'c':25,'d':{21:'abc',22:'qwerty'}}},'a':[1,2,3],'f': function(){return 12;},g(){return 23;},h:()=>12};
console.log(invert(testObject));

testObject = "'{ name: 'Bruce Wayne', age: 36, location: 'Gotham' }'";
console.log(invert(testObject));

testObject = null;
console.log(invert(testObject));

testObject = undefined;
console.log(invert(testObject));

testObject = {};
console.log(invert(testObject));

testObject = true;
console.log(invert(testObject));

testObject = [1234, 'qwerty', 'mnob'];
console.log(invert(testObject));

testObject = (abc,xyz) => abc+xyz;
console.log(invert(testObject));

testObject = new Set([1234, 'qwerty', 'mnob']);
console.log(invert(testObject));