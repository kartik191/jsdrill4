const mapObject = require('../mapObject.js');


let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(mapObject(testObject,(val, key) => val + val));

testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' ,23: 'values','o':{a:'a',b:'b'},'a':[1,2,3],'f': function(){return 12;},g(){return 23;}};
console.log(mapObject(testObject,(val, key) => val +val));

testObject = "'{ name: 'Bruce Wayne', age: 36, location: 'Gotham' }'";
console.log(mapObject(testObject,(val, key) => val + val));

testObject = null;
console.log(mapObject(testObject,(val, key) => val + val));

testObject = undefined;
console.log(mapObject(testObject,(val, key) => val + val));

testObject = {};
console.log(mapObject(testObject,(val, key) => val + val));

testObject = true;
console.log(mapObject(testObject,(val, key) => val + val));

testObject = [1234, 'qwerty', 'mnob'];
console.log(mapObject(testObject,(val, key) => val + val));

testObject = (abc,xyz) => abc+xyz;
console.log(mapObject(testObject,(val, key) => val + val));

testObject = new Set([1234, 'qwerty', 'mnob']);
console.log(mapObject(testObject,(val, key) => val + val));