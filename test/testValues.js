const values = require('../values.js');


let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(values(testObject));

testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' ,23: values,'o':{a:'a',b:'b'},'a':[1,2,3],'f': function(){return 12;},g(){return 23;},h:()=>12};
console.log(values(testObject));

testObject = "'{ name: 'Bruce Wayne', age: 36, location: 'Gotham' }'";
console.log(values(testObject));

testObject = null;
console.log(values(testObject));

testObject = undefined;
console.log(values(testObject));

testObject = 1234;
console.log(values(testObject));

testObject = true;
console.log(values(testObject));

testObject = [1234, 'qwerty', 'mnob'];
console.log(values(testObject));

testObject = (abc,xyz) => abc+xyz;
console.log(values(testObject));

testObject = new Set([1234, 'qwerty', 'mnob']);
console.log(values(testObject));