const pairs = require('../pairs.js');


let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(pairs(testObject));

testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' ,23: pairs,'o':{a:'a',b:'b'},'a':[1,2,3],'f': function(){return 12;},g(){return 23;},h:()=>12};
console.log(pairs(testObject));

testObject = "'{ name: 'Bruce Wayne', age: 36, location: 'Gotham' }'";
console.log(pairs(testObject));

testObject = null;
console.log(pairs(testObject));

testObject = undefined;
console.log(pairs(testObject));

testObject = {};
console.log(pairs(testObject));

testObject = true;
console.log(pairs(testObject));

testObject = [1234, 'qwerty', 'mnob'];
console.log(pairs(testObject));

testObject = (abc,xyz) => abc+xyz;
console.log(pairs(testObject));

testObject = new Set([1234, 'qwerty', 'mnob']);
console.log(pairs(testObject));