function values(obj) {
    if(typeof(obj) !== 'object' || obj === null || Array.isArray(obj)){
        return [];
    }
    let valuesArray = [];
    for(let key in obj){
        if(typeof(obj[key]) !== 'function'){
            valuesArray.push(obj[key]);
        }
    }
    return valuesArray;
}

module.exports = values;