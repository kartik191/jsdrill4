function defaults(obj, defaultProps){
    if(typeof(obj) !== 'object' || obj === null || Array.isArray(obj)){
        return {};
    }
    let newObj = obj;
    for(let key in defaultProps){
        if(newObj[key] === undefined){
            newObj[key] = defaultProps[key];
        }
    }
    return newObj; 
}

module.exports = defaults;