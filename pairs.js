function pairs(obj) {
    if(typeof(obj) !== 'object' || obj === null || Array.isArray(obj)){
        return [];
    }
    let arr = [];
    for(let key in obj){
        arr.push([key,obj[key]]);
    }
    return arr;
}

module.exports = pairs;