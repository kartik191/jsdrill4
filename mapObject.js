function mapObject(obj, cb) {
    if(typeof(obj) !== 'object' || obj === null || Array.isArray(obj)){
        return {};
    }
    let newObj = {};
    for(let key in obj){
        newObj[key] = cb(obj[key], key);
    }
    return newObj;
}

module.exports = mapObject;