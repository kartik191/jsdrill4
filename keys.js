function keys(obj){
    if(typeof(obj) !== 'object' || obj === null || Array.isArray(obj)){
        return [];
    }
    let keysArr = [];
    for(let key in obj){
        keysArr.push(key);
    }
    return keysArr;
}

module.exports = keys;