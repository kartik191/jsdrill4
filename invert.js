function invert(obj){
    if(typeof(obj) !== 'object' || obj === null || Array.isArray(obj)){
        return [];
    }
    let newObj = {};
    for(let key in obj){
        let value = obj[key];
        if(typeof(value) === 'object'){
            value = JSON.stringify(value);
        }
        newObj[value] = key;
    }
    return newObj;
}

module.exports = invert;